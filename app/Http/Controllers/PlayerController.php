<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use App\User;

class PlayerController extends Controller
{
    public function index($id)
    {
        return new UserResource(User::find($id));
    }

    public function showAll()
    {
        return UserResource::collection(User::all());
    }

    public function saveUser(Request $request)
    {
        $item = new User;
        $item->name = $request->name;
        $item->email = $request->email;
        $item->password = $request->password;
        $res = [];
        $res["name"] = $item->name;
        $res["email"] = $item->email;
        $res["password"] = $item->password;
        $item->save();
        return $res;

    }

    public function updateUser(Request $request , $id)
    {
        $player = User::find($id);
        if ($request->name != null) {
            $player->name = $request->name;
        }
        if ($request->email != null) {
            $player->email = $request->email;
        }
        if ($request->password != null) {
            $player->password = $request->password;
        }
        $res = [];
        $res["name"] = $player->name;
        $res["email"] = $player->email;
        $res["password"] = $player->password;
        $player->save();
        return $res;


    }

    public function delUser($id)
    {
        $player = User::find($id);
        $res = [];
        $res["name"] = $player->name;
        $res["email"] = $player->email;
        $res["password"] = $player->password;
        $player->delete();
        return $res;


    }

 }
