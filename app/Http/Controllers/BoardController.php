<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Board as BoardResource;
use App\User;
use App\Board;



class BoardController extends Controller
{

    private function ObjToArray($obj) {
        $res = [];
        $res["name"] = $obj->name;
        $res["game"] = $obj->game;
        $res["score"] = $obj->score;
        return $res;
    }

    private function leader()
    {
        $scoreBoard = [];
        $players = Board::orderBy('score', 'desc')
            ->select('name', 'score')
            ->get();

        $result = [];

        for ($i = 0; $i < 6; $i++) {     // Se aleg primii 6 jucatori cu cel mai mare scor

            $result[$i]["name"] = $players[$i]["name"];
            $result[$i]["score"] = $players[$i]["score"];
            $scoreBoard = array($result , $players);

         }
         return $scoreBoard;
    }


    public function history()
    {
        $user = Auth::user();
        $logName = $user["name"];
        $games = Board::where('name', $logName)
            ->select('game', 'score')
            ->get();

        return view('history' , ['games' => $games]);

    }

    public function boardSave(Request $request)
    {
        $item = new Board;
        $item->name = $request->name;
        $item->game = $request->game;
        $item->score = $request->score;
        $item->save();

    }

    public function leaderboard()
    {
        $topScore = $this->leader();

            $user = Auth::user();    // Se alege scorul userului logat
            $logName = $user["name"];
            $position = 1;
            $puncte = 0;
            foreach ($topScore[1] as $play) {
                if ($play->name == $logName) {
                    $puncte = $play->score;
                    break;
                }
                $position++;
            }

        return view('leaderboard' , ['result' => $topScore[0] , 'points' => $puncte , 'pos' => $position]);
    }

    public function index()
    {
        return BoardResource::collection(Board::all());
    }


    public function showHistory($player)
    {
        $games = BoardResource::collection(Board::where('name', $player)
            ->select('game', 'score')
            ->get());
        return $games;

    }

    public function showScore($player)
    {
        $game = new BoardResource(Board::orderBy('score', 'desc')
        ->where('name', $player)
        ->select('name', 'score')
        ->take(1)
        ->get());
        return $game[0];
    }

    public function showLeader()
    {
        $topScore = $this->leader();

        return $topScore[0];
    }

    public function saveBoard(Request $request)
    {
        $item = new Board;
        $item->name = $request->name;
        $item->game = $request->game;
        $item->score = $request->score;
        $result = $this->ObjToArray($item);
        $item->save();
        return $result;

    }

    public function updateBoard(Request $request , $id)
    {
        $game = Board::find($id);
        if ($request->name != null) {
          $game->name = $request->name;
        }
        if ($request->game != null) {
            $game->game = $request->game;
        }
        if ($request->score != null) {
            $game->score = $request->score;
        }
        $result = $this->ObjToArray($game);
        $game->save();
        return $result;


      }

      public function delBoard($id)
      {
          $game = Board::find($id);
          $result = $this->ObjToArray($game);
          $game->delete();
          return $result;


      }

    public function showToken()
    {
        return csrf_token();
    }

}
