<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fight leaderboard</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <style>
        .bac {background-color: darkblue;
              height: 700px;
             }
        .pan {background-color: lightgray;
            position: relative;
            width: 250px;
            top: 100px;
            margin: auto;
        }
        .lead {font-size: 150%}
        h3 {text-align: center;}
    </style>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 bac">
            <div class="pan">
                <h3>Leaderboard</h3>
                <ol class="lead">
                    @foreach ($result as $line)
                    <li>
                        <div class="row">
                          <div class="col-md">{{ $line["name"] }}</div>
                          <div class="col-md">{{ $line["score"] }}</div>
                        </div>
                    </li>
                    @endforeach
                </ol>
                <h4>&nbsp {{ $pos }}. Your score: {{ $points }}</h4>

                <h3><a href="{{ url("/home") }}">Back</a></h3><br>
            </div>

        </div>
    </div>
</div>

</body>
</html>