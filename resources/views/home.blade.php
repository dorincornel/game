@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h4>Fight</h4></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h4>Hello {{ Auth::user()->name }}</h4>
                    You are logged in!
                </div>
            </div>
            <div style="position:relative;float:left;font-size: large;">
                <a href="{{ url('/home/history') }}">Game history</a>
            </div>
            <div style="position:relative;float:right;font-size: large;">
                <a href="{{ url('/home/leaderboard') }}">Leaderboard</a>
            </div>
        </div>
    </div>
</div>
@endsection
