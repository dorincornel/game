<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fight history</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <style>
        .bac {background-color: darkblue; height:700px;}
        .pan {background-color: lightgray;
            position: relative;
            width: 200px;
            top: 100px;
            margin: auto;
        }
        .hist {list-style-type:none; font-size: 150%}
        h3 {text-align: center;}
    </style>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 bac">
            <div class="pan">
                <h3>Game history</h3>
                <ul class="hist">
                    @foreach ($games as $line)
                        <li>
                            {{ $line->game }} &nbsp&nbsp {{ $line->score }}
                        </li>
                    @endforeach
                </ul>
                <h3><a href="{{ url("/home") }}">Back</a></h3><br>
            </div>

        </div>
    </div>
</div>

</body>
</html>