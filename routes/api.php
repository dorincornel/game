<?php

use Illuminate\Http\Request;
use App\Board;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/leaderboard', 'BoardController@showLeader');

Route::get('/user/{id}', 'PlayerController@index');

Route::get('/users', 'PlayerController@showAll');

Route::get('/score/{player}', 'BoardController@showScore');

Route::get('/history/{player}', 'BoardController@showHistory');

Route::get('/board', 'BoardController@index');

Route::post('/post', 'BoardController@saveBoard');

Route::put('/put/{id}', 'BoardController@updateBoard');

Route::delete('/delete/{id}', 'BoardController@delBoard');

Route::post('/userAdd', 'PlayerController@saveUser');

Route::put('/userUp/{id}', 'PlayerController@updateUser');

Route::delete('/userDel/{id}', 'PlayerController@delUser');





